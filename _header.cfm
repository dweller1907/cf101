<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title><cfoutput>#application.siteName#</cfoutput></title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="http://getbootstrap.com/examples/navbar-fixed-top/navbar-fixed-top.css">

<cfinclude template="helper.cfm">
</head>

<body>

 <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.cfm">CF101</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
          <cfset activeFile = cgi.SCRIPT_NAME>
            <li <cfif activeFile CONTAINS 'index'>class="active"</cfif>><a href="index.cfm">Anasayfa</a></li>
            <li <cfif activeFile CONTAINS 'ekle'>class="active"</cfif>><a href="ekle.cfm">Ekle</a></li>
            <li <cfif activeFile CONTAINS 'listele'>class="active"</cfif>><a href="listele.cfm">Listele</a></li>
            
          </ul>
   
        </div><!--/.nav-collapse -->
      </div>
    </div>
<div class="container">

