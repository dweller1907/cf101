<cfscript>
/**
 * Strips HTML from all form fields.
 * Version 1.1 by Raymond Camden
 * 
 * @param nostrip 	 List of form fields that should not be modified. 
 * @return Returns true. 
 * @author Douglas Williams (&#107;&#108;&#101;&#110;&#122;&#97;&#100;&#101;&#64;&#97;&#118;&#111;&#110;&#100;&#97;&#108;&#101;&#46;&#99;&#111;&#109;) 
 * @version 1.1, December 19, 2001 
 */
function FormStripHTML() {
	var nostrip = "";
	if(arraylen(Arguments)) nostrip = Arguments[1];

        if(structIsEmpty(form)) return;

	for (field in form) {
   		if(NOT listfindnocase(nostrip, field)) form[field] = ReReplaceNoCase(form[field], "<[^>]*>", "", "ALL");
	}

        return true;
}
</cfscript>

<cfscript>
/**
 * This function returns the number of lines in a text file.
 * 
 * @param text 	 String to parse. (Required)
 * @return Returns a number. 
 * @author Matheus Antonelli (&#109;&#97;&#116;&#104;&#101;&#117;&#115;&#95;&#97;&#110;&#116;&#111;&#110;&#101;&#108;&#108;&#105;&#64;&#105;&#103;&#46;&#99;&#111;&#109;&#46;&#98;&#114;) 
 * @version 1, June 3, 2003 
 */
function CountLines(text) {
  var CRLF = Chr(13) & Chr(10);
  return ListLen(text,CRLF);
}
</cfscript>