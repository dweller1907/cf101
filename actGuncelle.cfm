<cfinclude template="_header.cfm" runonce="yes">


<cfif Isdefined("form.guncelle")>

<cffile action = "upload" 
        fileField = "foto" 
        destination = "c:\temp\" 
        accept = "image/png, image/jpg, image/jpeg, image/gif" 
        nameConflict = "MakeUnique">

<cfdump var="#cffile#">

<!--- TODO: Dizinleri otomatik tanı  --->

<cfset newFilename = Dateformat(now(),'YYYYMMDD') & timeformat(now(),'HHMMSS') & "." & cffile.clientfileext>
<cffile action="move" destination="C:\sites\egitim\web\img\#newFilename#" source="c:\temp\#cffile.clientfile#">

    <cfquery name="ekle" datasource="geotur">
           UPDATE	users
           SET		ad = <cfqueryparam value="#form.ad#" cfsqltype="CF_SQL_VARCHAR">,
           			ilid = 	<cfqueryparam value="#form.sehirid#" cfsqltype="CF_SQL_NUMERIC">,
                    foto = <cfqueryparam value="#newFilename#" cfsqltype="CF_SQL_VARCHAR">
           WHERE	id = <cfqueryparam value="#id#" cfsqltype="CF_SQL_NUMERIC">
    </cfquery>

    Bilgileriniz kaydedildi! 
</cfif>

<cflocation url="listele.cfm" addtoken="no">


<cfinclude template="_footer.cfm" runonce="yes">

