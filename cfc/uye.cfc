<cfcomponent output="no">
	<cffunction access="public" name="getCities" returntype="query">
        <cfset var qCities = "">
        <cfquery name="qCities" datasource="geotur">
            SELECT  il_id   AS placeID, 
                    il_adi  AS placeName, 
                    lat, 
                    lng
            FROM    pk_il
            ORDER by il_adi        
        </cfquery>        
		<cfreturn qCities>
	</cffunction>
    
    <cffunction access="public" name="getUsers" returntype="query">
    	<cfargument name="id" type="numeric" required="no" default="0">
        <cfset var qUsers = "">
        <cfquery name="qUsers" datasource="geotur">
      		SELECT  id, ad, notes, u.ilid, p.il_adi, foto
    		FROM    users u, pk_il p
    		WHERE   u.ilid = p.il_id
            		<cfif arguments.id>
					AND
                    id = <cfqueryparam value="#arguments.id#" cfsqltype="CF_SQL_NUMERIC">
					</cfif>
    		ORDER by dateUpdated DESC       
        </cfquery>        
		<cfreturn qUsers>
	</cffunction>
    
    <cffunction access="public" name="deleteUser" output="no" returntype="boolean" verifyclient="no" securejson="false">
    	<cfargument name="id" type="numeric" required="yes" default="0">
        <cfset var dUser = "">
        <cfquery name="dUser" datasource="geotur">
      		 DELETE FROM users
            WHERE	id = <cfqueryparam value="#id#" cfsqltype="CF_SQL_NUMERIC">     
        </cfquery>        
		<cfreturn 1>
	</cffunction>
</cfcomponent>