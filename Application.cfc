<cfcomponent displayname="Application" output="false" hint="AppTest">
 
	<cfset this.name = "AppTestdssssssdddd" />
	<cfset this.sessionManagement = true />
	<cfset this.setClientCookies = false />
	<cfset this.applicationTimeout = createTimeSpan( 0, 1, 0, 0 ) />
    <cfset this.sessionTimeout = createTimeSpan( 0, 0, 1, 0 ) />

 	<cfsetting requesttimeout="200"  enablecfoutputonly="false" />
 
 	<cffunction name="OnApplicationStart" access="public" returntype="boolean" output="false" hint="App Parameters">
		<cfscript>
        	application.dsn = 'AppTest';
			application.siteName = 'Test Site';			
			application.activeSessions = {};
			application.objUye = createObject("component", "cfc.uye");
        </cfscript>
		<cfreturn true />
	</cffunction>
    
    
	<cffunction name="OnSessionStart" access="public" returntype="void" output="false" hint="Session Parameters">
 		<cfscript>
        	session.isLogged = 0;
			session.isAdmin = 0;
			session.uuid = createUUID();			
			application.activeSessions[ session.uuid ] = session;
        </cfscript>
		<!--- Return out. --->
		<cfreturn />
	</cffunction>
    
	<cffunction name="OnRequestStart" access="public" returntype="boolean" output="false" hint="Request Starts">
		<cfargument name="TargetPage" type="string" required="true" />
        
        
 
 		<cfscript>
			request.objUye = createObject("component", "cfc.uye");
			request.tickStart = #getTickCount()#;
        </cfscript>
		<cfreturn true />
	</cffunction>
    
<cffunction name="onSessionEnd" access="public" returntype="void" output="false" hint="I clean up a session.">
 
	<!--- Define arguments. --->
	<cfargument name="session" type="any" required="true" hint="I am the session that is ending." />
 
	<cfargument name="application" type="any" required="true" hint="I am the application scope for the given session." />
 
	<cfset structDelete( arguments.application.activeSessions, arguments.session.uuid ) />
 
	<!--- Return out. --->
	<cfreturn />
</cffunction>
 
 
 <!--- 
 
 
	<cffunction
		name="OnRequest"
		access="public"
		returntype="void"
		output="true"
		hint="Fires after pre page processing is complete.">
 
		<!--- Define arguments. --->
		<cfargument
			name="TargetPage"
			type="string"
			required="true"
			/>
 
		<!--- Include the requested page. --->
		<cfinclude template="#ARGUMENTS.TargetPage#" />
 
		<!--- Return out. --->
		<cfreturn />
	</cffunction>
 	 --->
 
	<cffunction
		name="OnRequestEnd"
		access="public"
		returntype="void"
		output="true"
		hint="Fires after the page processing is complete.">
        
        <cfscript>
        	request.tickEnd = #getTickCount()#;
			request.totalTime = request.tickEnd - request.tickStart;
        </cfscript>
 
		<!--- Return out. --->
		<cfreturn />
	</cffunction>
 
 

 
 
	<cffunction
		name="OnApplicationEnd"
		access="public"
		returntype="void"
		output="false"
		hint="Fires when the application is terminated.">
 
		<!--- Define arguments. --->
		<cfargument
			name="ApplicationScope"
			type="struct"
			required="false"
			default="#StructNew()#"
			/>
 
		<!--- Return out. --->
		<cfreturn />
	</cffunction>
 
 <!---
 
	<cffunction
		name="OnError"
		access="public"
		returntype="void"
		output="true"
		hint="Fires when an exception occures that is not caught by a try/catch.">
 
		<!--- Define arguments. --->
		<cfargument
			name="Exception"
			type="any"
			required="true"
			/>
 
		<cfargument
			name="EventName"
			type="string"
			required="false"
			default=""
			/>
 
		<!--- Return out. --->
		<cfreturn />
	</cffunction>
 --->
</cfcomponent>